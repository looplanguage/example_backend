//! This backend will interpret the AST output from Vinci and execute it
//! Not the full feature set of Arc will be supported, like library loading

use std::collections::HashMap;
use std::ops::Deref;
use vinci::ast::instructions::memory::LoadType;
use vinci::ast::instructions::suffix::BinaryOperation;
use vinci::ast::instructions::Node;
use vinci::parse;
use vinci::types::{Type, ValueType};

#[derive(Debug, PartialEq, Clone)]
pub enum Types {
    Integer(i64),
    Boolean(bool),
    Array(Box<Vec<Types>>),
    Function(Box<RuntimeFunction>),
    Character(char),
    Null,
    None,
}

#[cfg(test)]
mod tests {
    use crate::{ExampleBackend, Types};

    #[test]
    fn test_math_simple() {
        let result = ExampleBackend::new().run(".ADD { .CONSTANT INT 10; .CONSTANT INT 20; };");

        assert_test(result, Types::Integer(30));
    }

    #[test]
    fn test_math_advanced() {
        // ((10 + 20) * 100) / 2
        let result = ExampleBackend::new().run(".DIVIDE { .MULTIPLY { .ADD { .CONSTANT INT 10; .CONSTANT INT 20; }; .CONSTANT INT 100; }; .CONSTANT INT 2; };");

        assert_test(result, Types::Integer(1500));
    }

    #[test]
    fn test_conditional_true() {
        let result = ExampleBackend::new().run(".IF CONDITION { .CONSTANT BOOL true; } THEN { .CONSTANT INT 10; } ELSE { .CONSTANT INT 20; };");

        assert_test(result, Types::Integer(10));
    }

    #[test]
    fn test_conditional_false() {
        let result = ExampleBackend::new().run(".IF CONDITION { .CONSTANT BOOL false; } THEN { .CONSTANT INT 10; } ELSE { .CONSTANT INT 20; };");

        assert_test(result, Types::Integer(20));
    }

    #[test]
    fn test_store_load() {
        let result = ExampleBackend::new()
            .run(".STORE 0 { .CONSTANT INT 20; }; .ADD { .LOAD VARIABLE 0; .LOAD VARIABLE 0; };");

        assert_test(result, Types::Integer(40));
    }

    #[test]
    fn test_call_function() {
        let result = ExampleBackend::new().run(".FUNCTION 0 INT ARGUMENTS { INT; } FREE { } THEN { .ADD { .LOAD PARAMETER 0; .LOAD PARAMETER 0; }; }; .ADD { .CALL local::0 { .CONSTANT INT 20; }; .CALL local::0 { .CONSTANT INT 20; }; };");

        assert_test(result, Types::Integer(80))
    }

    #[test]
    fn test_call_function_2() {
        let result = ExampleBackend::new().run(".FUNCTION 0 INT ARGUMENTS { INT; } FREE { } THEN { .ADD { .LOAD PARAMETER 0; .LOAD PARAMETER 0; }; }; .FUNCTION 1 INT ARGUMENTS { INT; } FREE { } THEN { .MULTIPLY { .LOAD PARAMETER 0; .LOAD PARAMETER 0; }; }; .ADD { .CALL local::0 { .CONSTANT INT 20; }; .CALL local::1 { .CONSTANT INT 5; }; };");

        assert_test(result, Types::Integer(65))
    }

    #[test]
    fn test_while() {
        let result = ExampleBackend::new().run(".STORE 0 { .CONSTANT INT 0; }; .WHILE CONDITION { .GREATERTHAN { .CONSTANT INT 10; .LOAD VARIABLE 0; }; } THEN { .STORE 0 { .ADD { .LOAD VARIABLE 0; .CONSTANT INT 1; }; }; }; .LOAD VARIABLE 0;");

        assert_test(result, Types::Integer(10))
    }

    #[test]
    fn test_array() {
        let result = ExampleBackend::new().run(".CONSTANT INT[] [.CONSTANT INT 10;];");

        assert_test(result, Types::Array(Box::new(vec![Types::Integer(10)])));
    }

    #[test]
    fn test_array_index() {
        let result = ExampleBackend::new().run(
            ".INDEX {.CONSTANT INT[] [.CONSTANT INT 10;.CONSTANT INT 20;];} {.CONSTANT INT 0;};",
        );

        assert_test(result, Types::Integer(10));
    }

    #[test]
    fn test_array_push() {
        let result = ExampleBackend::new().run(
            ".PUSH {.CONSTANT INT[] [.CONSTANT INT 10;.CONSTANT INT 20;];} {.CONSTANT INT 30;};",
        );

        assert_test(
            result,
            Types::Array(Box::new(vec![
                Types::Integer(10),
                Types::Integer(20),
                Types::Integer(30),
            ])),
        );
    }

    #[test]
    fn test_array_slice() {
        let result = ExampleBackend::new().run(
            ".SLICE {.CONSTANT INT[] [.CONSTANT INT 10;.CONSTANT INT 20;.CONSTANT INT 30;];} {.CONSTANT INT 0;} {.CONSTANT INT 2;};",
        );

        assert_test(
            result,
            Types::Array(Box::new(vec![Types::Integer(10), Types::Integer(20)])),
        );
    }

    #[test]
    fn test_array2d() {
        let result =
            ExampleBackend::new().run(".CONSTANT INT[][] [.CONSTANT INT[] [.CONSTANT INT 10;.CONSTANT INT 20;];.CONSTANT INT[] [.CONSTANT INT 30;.CONSTANT INT 40;];];");

        assert_test(
            result,
            Types::Array(Box::new(vec![
                Types::Array(Box::new(vec![Types::Integer(10), Types::Integer(20)])),
                Types::Array(Box::new(vec![Types::Integer(30), Types::Integer(40)])),
            ])),
        );
    }

    #[test]
    fn test_strings() {
        let result = ExampleBackend::new().run(".CONSTANT CHAR[] \"Hello World!\";");

        assert_test(
            result,
            Types::Array(Box::new(vec![
                Types::Character('H'),
                Types::Character('e'),
                Types::Character('l'),
                Types::Character('l'),
                Types::Character('o'),
                Types::Character(' '),
                Types::Character('W'),
                Types::Character('o'),
                Types::Character('r'),
                Types::Character('l'),
                Types::Character('d'),
                Types::Character('!'),
            ])),
        )
    }

    fn assert_test(result: Result<Types, ()>, expected: Types) {
        if let Ok(result) = result {
            assert_eq!(result, expected)
        } else {
            assert!(false)
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct RuntimeFunction {
    free: Vec<Types>,
    parameters: Vec<Types>,
    body: Vec<Node>,
}

pub struct ExampleBackend {
    variables: HashMap<u64, Types>,
    functions: HashMap<String, RuntimeFunction>,
    function_stack: Vec<RuntimeFunction>,
}

impl ExampleBackend {
    pub fn new() -> ExampleBackend {
        ExampleBackend {
            variables: HashMap::new(),
            functions: Default::default(),
            function_stack: Vec::new(),
        }
    }

    pub fn run(&mut self, code: &str) -> Result<Types, ()> {
        let ast = parse(code);

        let mut last_result: Types = Types::None;
        for node in ast.nodes {
            last_result = self.execute_expression(node);
        }

        Ok(last_result)
    }

    fn execute_body(&mut self, nodes: Vec<Node>) -> Types {
        let mut last = Types::None;

        for node in nodes {
            last = self.execute_expression(node);
        }

        last
    }

    fn execute_array(&mut self, items: Vec<ValueType>) -> Types {
        let mut result = vec![];

        for item in items {
            match item {
                ValueType::Integer(integer) => result.push(Types::Integer(integer)),
                ValueType::Boolean(bool) => result.push(Types::Boolean(bool)),
                ValueType::Array(it) => result.push(self.execute_array(*it)),
                ValueType::Character(char) => result.push(Types::Character(char)),
            }
        }

        Types::Array(Box::new(result))
    }

    fn execute_expression(&mut self, node: Node) -> Types {
        match node {
            Node::CONSTANT(constant) => match constant {
                ValueType::Integer(int) => Types::Integer(int),
                ValueType::Boolean(bool) => Types::Boolean(bool),
                ValueType::Array(arr) => self.execute_array(*arr),
                ValueType::Character(char) => Types::Character(char)
            },
            Node::LOAD(load) => match load.load_type {
                LoadType::VARIABLE => {
                    let value = self.variables.get(&load.index).unwrap().clone();

                    value
                }
                LoadType::PARAMETER => {
                    let func = self.function_stack.first().unwrap().clone();
                    let param = func.parameters.get(load.index as usize);

                    param.unwrap().clone()
                }
            },
            Node::STORE(store) => {
                let value = self.execute_expression(*store.value);
                self.variables.insert(store.index, value);
                Types::None
            }
            Node::SUFFIX(suffix) => {
                let left = self.execute_expression(suffix.left);
                let right = self.execute_expression(suffix.right);

                if let (Types::Integer(right), Types::Integer(left)) = (right, left) {
                    match suffix.operation {
                        BinaryOperation::ADD => Types::Integer(left + right),
                        BinaryOperation::SUBTRACT => Types::Integer(left - right),
                        BinaryOperation::MULTIPLY => Types::Integer(left * right),
                        BinaryOperation::DIVIDE => Types::Integer(left / right),
                        BinaryOperation::POWER => Types::Integer(left.pow(right as u32)),
                        BinaryOperation::GREATERTHAN => Types::Boolean(left > right),
                        BinaryOperation::EQUALS => Types::Boolean(left == right),
                        BinaryOperation::NOTEQUALS => Types::Boolean(left != right),
                    }
                } else {
                    Types::None
                }
            }
            Node::CONDITIONAL(conditional) => {
                let condition_result = self.execute_expression(conditional.condition);

                if let Types::Boolean(bool) = condition_result {
                    if bool {
                        self.execute_body(conditional.body)
                    } else {
                        self.execute_body(conditional.alternative)
                    }
                } else {
                    Types::None
                }
            }
            Node::FUNCTION(func) => {
                let name = format!("local::{}", func.index);
                let body = func.body;
                let mut parameters: Vec<Types> = Vec::new();
                for parameter in func.parameters {
                    match parameter {
                        Type::INT => {
                            parameters.push(Types::Integer(0));
                        }
                        Type::BOOL => {
                            parameters.push(Types::Boolean(false));
                        }
                        _ => {
                            panic!(
                                "The example backend does not support all types: {:?}",
                                parameter
                            )
                        }
                    }
                }

                let runtime = RuntimeFunction {
                    free: vec![],
                    parameters,
                    body,
                };

                self.functions.insert(name, runtime);

                Types::None
            }
            Node::CALL(call) => {
                let mut parameters: Vec<Types> = Vec::new();

                for argument in call.arguments {
                    parameters.push(self.execute_expression(argument));
                }

                let function = self.functions.get_mut(call.namespace.as_str()).unwrap();

                let mut cloned = function.clone();

                cloned.parameters = parameters;

                let body = function.body.clone();

                self.function_stack.push(cloned);

                let result = self.execute_body(body);

                self.function_stack.pop();

                result
            }
            Node::WHILE(wh) => {
                let mut result = Types::None;

                while self.execute_expression(wh.condition.clone()) == Types::Boolean(true) {
                    result = self.execute_body(wh.body.clone());
                }

                result
            }
            Node::INDEX(index) => {
                let to_index = self.execute_expression(*index.to_index);
                let index = self.execute_expression(*index.index);

                if let (Types::Array(arr), Types::Integer(index)) = (to_index, index) {
                    let item = arr.get(index as usize);

                    if let Some(item) = item.cloned() {
                        item
                    } else {
                        Types::None
                    }
                } else {
                    Types::None
                }
            }
            Node::PUSH(push) => {
                let to_push = self.execute_expression(*push.to_push);
                let item = self.execute_expression(*push.item);

                if let Types::Array(arr) = to_push {
                    let mut clone = *arr.clone();
                    clone.push(item);
                    Types::Array(Box::new(clone))
                } else {
                    Types::None
                }
            }
            Node::SLICE(slice) => {
                let to_slice = self.execute_expression(*slice.to_slice);
                let from = self.execute_expression(*slice.from);
                let till = self.execute_expression(*slice.to);

                if let (Types::Array(arr), Types::Integer(from), Types::Integer(till)) =
                    (to_slice, from, till)
                {
                    let cloned = arr.clone().deref().clone();
                    let test = &cloned[from as usize..till as usize];
                    Types::Array(Box::from(test.clone().to_vec()))
                } else {
                    Types::None
                }
            }
            Node::COPY(_) => Types::None,
            Node::LOADLIB(_) => Types::None,
        }
    }
}
